/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Agenda.modelo.Pessoa;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author alexandre
 */
public class pessoaTest {
    
	private Pessoa pessoaTest;
	
	@Before
	public void setUp() throws Exception{
		Pessoa pessoaTest = new Pessoa();
	}
	
	@Test
	public void testNome() {
		Pessoa pessoaTest = new Pessoa();
		pessoaTest.setNome("Alexandre");
		assertEquals(pessoaTest.getNome(), "Alexandre");
		
	}
        public void testIdade() {
		Pessoa pessoaTest = new Pessoa();
		pessoaTest.setIdade("18");
		assertEquals(pessoaTest.getIdade(), "18");
		
	}
	@Test
	public void testTelefone() {
		Pessoa pessoaTest = new Pessoa();
		pessoaTest.setTelefone("xxxx-xxxx");
		assertEquals(pessoaTest.getTelefone(), "xxxx-xxxx");
		
	}

	@Test
	public void testSexo() {
		Pessoa pessoaTest = new Pessoa();
		pessoaTest.setSexo("M");
		assertEquals(pessoaTest.getSexo(), "M");
		
	}
	
	@Test
	public void testHangout() {
		Pessoa pessoaTest = new Pessoa();
		pessoaTest.setHangout("xxxx-xxxx");
		assertEquals(pessoaTest.getHangout(), "xxxx-xxxx");
		
	}
	
	@Test
	public void testEmail() {
		Pessoa pessoaTest = new Pessoa();
		pessoaTest.setEmail("xx@xx");
		assertEquals(pessoaTest.getEmail(), "xx@xx");
		
	}
	
	@Test
	public void testEndereco() {
		Pessoa pessoaTest = new Pessoa();
		pessoaTest.setEndereco("xxx");
		assertEquals(pessoaTest.getEndereco(), "xxx");
		
	}
	
	@Test
	public void testCpf() {
		Pessoa pessoaTest = new Pessoa();
		pessoaTest.setCpf("123456");
		assertEquals(pessoaTest.getCpf(), "123456");
		
	}
	
	@Test
	public void testRg() {
		Pessoa pessoaTest = new Pessoa();
		pessoaTest.setRg("123456");
		assertEquals(pessoaTest.getRg(), "123456");
	}
}
